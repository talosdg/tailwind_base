# BASE PARA PROYECTO TAILWINDCSS 

## Jornadas de Visibilidad Web UNAM 2022
Este proyecto es una instalación básica de Tailwindcss para comenzar a trabajar sin preocuparse por la configuración.

[Descarga](https://gitlab.com/talosdg/tailwind_base/-/archive/main/tailwind_base-main.zip) el proyecto haciendo clic en el icono de descarga.

